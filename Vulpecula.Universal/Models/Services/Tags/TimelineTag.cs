﻿using Vulpecula.Universal.Models.Timelines;

namespace Vulpecula.Universal.Models.Services.Tags
{
    public class TimelineTag
    {
        public TimelineType Type { get; set; }

        public long Id { get; set; }
    }
}