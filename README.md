# Croudia Client "Vulpecula"
This app is cross-platform application for [Croudia](https://croudia.com).  
Supported Platform is:
* Universal Windows Platforms
* Windows Desktop
* Android
* iOS
* Mac (TBI)


## Projects
* Vulpecula.Android
* Vulpecula.Desktop
* Vulpecula.Mobile (Shared)
* Vulpecula.Streaming (Shared)
* Vulpecula.Universal
* Vulpecula.Universal.BgTask
* Vulpecula.iOS
* Vulpecula (Shared)